const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', (event) => {
	getFullName();
});

function getFullName(){

	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;

}

txtLastName.addEventListener('keyup', (event) => {
	getFullName();
});
