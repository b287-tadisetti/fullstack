//The "document" refers to the whole webpage.
//The "querySelector" is used to select a specific object (html element) from the document(webpage)

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

//Alternatively, we can use the getElement functions to retrieve the element
	//document.getElementById
	//document.getElementByClassName
	//document.getElementByTagName

//Whenever a user intercats with a webpage, this action is considered as an event.
//The "addEventListener" is function that takes two arguments,
	//string idnetifying the event (keyup)
	//fuction that the listener will execute once the specified event is triggered(event)
txtFirstName.addEventListener('keyup', (event) => {
	//The innerHTML property sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	//The "event.target" contains the element where the event happened
	console.log(event.target);
	//The "event.target.avlue" gets the value of the input objects (similar to the textFirstName.value);
	console.log(event.target.value)
})

