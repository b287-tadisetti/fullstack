Session 50:
	create a s50-s55 folder
		inside the folder run, npx create-react-app react-app
				Wait for the "Happy hacking" sign
		cd react-app
				touch guide.js
				npm start, to start the application
				remove the unnecessary files:
					App.test.js
					index.css
					logo.svg
					reportWebVitals.js
				install bootstrap dependencies
					npm install react-bootstrap bootstrap
				create a components folder in src
					create AppNavbar.js
					create Banner.js
					create Highlights.js
					refactor App.css
				create a pages folder in src
					create Home.js
				Activity
					create CourseCard.js
Session 51:
	create a data folder inside src
	create a Courses.js in pages
		create a stateHook for the count
Session 52
		refractor CourseCard.js
		create a Register page
				Utilize a conditional render based on isActive state
		Activity
			create a Login.js

Session 53
		install react-router-dom
		generate Route in App.js
		refractor link in AppNavbar.js
		create a Logout.js
		Activity
				create a wildcard character for the Error.js
Session 54
		create and utilize useState
		create UserContext.js
		utilize UserProvider
		Activity
		 	refractor Register.js
Session 55
		install sweetalert2
		utilize fetch for our request, using our API Development
		refractor Login.js
		fetch Courses from database
		Activity
				refractor Register page
						utilize enroll request 
		