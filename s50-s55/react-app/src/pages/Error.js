/*import { Button, Row, Col } from 'react-bootstrap';

export default function Error() {
	
	return(
		<Row>
			<Col>
				<h1>Error 404 - Page Not Found!</h1>
				<p>The page you are looking for cannot be found.</p>
				<Button variant="primary"> Back to Home </Button>
			</Col>
		</Row>
	)
};*/

import Banner from '../components/Banner';

export default function Error() {

	const data = {
		title: "Error 404 - Page Not Found!",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return(
		<Banner data={data} />
	)
};