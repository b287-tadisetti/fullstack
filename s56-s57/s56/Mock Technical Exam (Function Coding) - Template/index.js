function countLetter(letter, sentence) {
    let count=0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    if(letter.length === 1){
        for(const char of sentence){
            if(letter === char){
                count++;
            }
        }
        return count;
    } 
    if(letter == "invalid"){
        return "undefined";
    }
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    for(let i=0; i<text.length-1; i++){
        let l=text[i];
        for(let j=i+1; j<text.length; j++){
            if(l==text[j]){
                return false;
            }
        }
    }
    return true;
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
    if(age < 13){
        return undefined;
    } else if((age>=13 && age<=21) || age>64){
        let num= price-(20/100)*price;
        let str=(num.toFixed(2)).toString();
        return str;
    } else{
        let num= price;
        let str=(num.toFixed(2)).toString();
        return str;
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let arr = [];
    let a = [];
    let k=0,r=0;
    items.forEach((item) => {
        if(item.stocks === 0){
            if(arr.length>0){
                for(let i=0; i<arr.length; i++){
                    if(item.category===arr[i]){
                        r=1;
                        break;
                    }
                }
                if(r!=1){
                    arr.push(item.category);
                }

            }else{
                arr.push(item.category);
            }
            
            
        }
    })
    return arr;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let common = [];
    let i=0;
    for(const voterA of candidateA){
        for(const voterB of candidateB){
            if(voterA === voterB){
                common[i] = voterA;
                i++;
                break;
            }
        }
    }
    return common;

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};